import telebot
import json
import requests
import os.path

with open(".env", "r") as file:
    TBOT_TOKEN = file.read().strip()
bot = telebot.TeleBot(TBOT_TOKEN)


class CurrencyRate:
    def __init__(self):
        self.rates_data = self._get_rates_data()

    @staticmethod
    def _get_rates_data():
        url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
        response = requests.get(url)
        if response == 200:
            return response.json()
        return []

    def get_rate(self, currency_code):
        for rate in self.rates_data:
            if rate['cc'] == currency_code:
                return rate['rate']
        return None

    def convert_currency(self, amount, from_currency, to_currency):
        if from_currency == 'UAH':
            rate_to = self.get_rate(to_currency)
            if rate_to:
                return amount / rate_to
        elif to_currency == 'UAH':
            rate_from = self.get_rate(from_currency)
            if rate_from:
                return amount * rate_from
        else:
            rate_from = self.get_rate(from_currency)
            rate_to = self.get_rate(to_currency)
            if rate_from and rate_to:
                return amount * (rate_from / rate_to)
            return None


currency_rate = CurrencyRate()


def save_conversion(conversion_data):
    if os.path.exists('conversions.json'):
        with open('conversions.json', 'r') as file:
            data = json.load(file)
    else:
        data = []

    data.append(conversion_data)

    if len(data) > 10:
        data = data[-10:]

    with open('conversion.json', 'w') as file:
        json.dump(data, file, index=4)


@bot.message_handler(commands=['start', 'help', 'hello'])
def handle_command(message):
    if message.text == '/start':
        bot.reply_to(message, 'Привіт! Вітаю вас.')
    elif message.text == '/help':
        bot.reply_to(message,
                     'Привіт.Я бот для конвертації валюти.Ви можете ввести суму та вихідну валюту,в яку хочете '
                     'конвертуватиВведіть чумму та натисніть кнопку для конвертації.')
    elif message.text == '/hello':
        bot.reply_to(message, 'Привіт! Вітаю вас.')

        main_menu(message)


def main_menu(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.row('Конвертувати валюту', 'Допомога')
    bot.send_message(message.chat.id, 'Оберіть функцію', reply_markup=keyboard)


@bot.message_handler(func=lambda message: message.text == 'Допомога')
def show_me_help(message):
    bot.reply_to(message,
                 'Введіть суму та валюту,в яку хочете конвертувати.Наприклад "100 USD" та виберіть валюту для '
                 'конвертації')


@bot.message_handler(func=lambda message: message.text == 'Конвертувати валюту')
def ask_amount_and_currency(message):
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, one_time_keyboard=True, resize_keyboard=True)
    btn1 = telebot.types.KeyboardButton('USD')
    btn2 = telebot.types.KeyboardButton('EUR')
    btn3 = telebot.types.KeyboardButton('UAH')
    markup.add(btn1, btn2, btn3)
    msg = bot.send_message(message.chat.id, 'Виберіть валюту з якої хочете конвертувати: ', reply_markup=markup)
    bot.register_next_step_handler(msg, process_base_currency)


def process_base_currency(message):
    base_currency = message.text.upper()
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, one_time_keyboard=True, resize_keyboard=True)
    btn1 = telebot.types.KeyboardButton('USD')
    btn2 = telebot.types.KeyboardButton('EUR')
    btn3 = telebot.types.KeyboardButton('UAH')
    markup.add(btn1, btn2, btn3)
    msg = bot.send_message(message.chat.id, f'Виберiть валюту в яку конвертувати {base_currency}: ',
                           reply_markup=markup)
    bot.register_next_step_handler(msg, lambda m: process_target_currency(m, base_currency))


def process_target_currency(message, base_currency):
    target_currency = message.text.upper()
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, one_time_keyboard=True, resize_keyboard=True)
    msg = bot.send_message(message.chat.id, f'Введіть суму для конвертації з {base_currency} в {target_currency}:',
                           reply_markup=markup)
    bot.register_next_step_handler(msg, lambda m: convert_currency(m, base_currency, target_currency))


def convert_currency(message, base_currency, target_currency):
    amount = float(message.text)
    convert_amount = currency_rate.convert_currency(amount, base_currency, target_currency)
    bot.send_message(message.chat.id, f'{amount} {base_currency} = {convert_amount:.2f} {target_currency}')
    conversion_data = {
        "amount": base_currency,
        "base_currency": target_currency,
        "convert_amount": convert_amount
    }
    save_conversion(conversion_data)


@bot.message_handler(func=lambda message: True)
def handler_conversion(message):
    parts = message.text.split()
    amount = float(parts[0])
    from_currency = parts[1].upper()
    keyboard = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    keyboard.row('USD', 'EUR', 'UAH')
    bot.send_message(message.chat.id, 'Виберіть валюту для конвертації: ', reply_markup=keyboard)
    bot.register_next_step_handler(message, handle_target_currency, amount, from_currency)


def handle_target_currency(message, amount, from_currency):
    to_currency = message.text.upper()
    converted_amount = currency_rate.convert_currency(amount, from_currency, to_currency)
    bot.send_message(message.chat.id, f"{amount} {from_currency} = {converted_amount:.2f} {to_currency}")
    conversion_data = {
        "amount": amount,
        "base_currency": from_currency,
        "target_currency": to_currency,
        "convert_amount": converted_amount
    }
    save_conversion(conversion_data)


bot.polling()
