import time


# Проста реалізація для перевірки простих чисел
def is_prime_simple(n):
    if n <= 1:
        return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


# Метод Решето Ератосфена
def sieve_of_eratosthenes(n):
    primes = [True] * (n + 1)
    p = 2
    while (p * p <= n):
        if primes[p]:
            for i in range(p * p, n + 1, p):
                primes[i] = False
        p += 1
    prime_numbers = [p for p in range(2, n + 1) if primes[p]]
    return prime_numbers


# Функція для вимірювання часу виконання
def measure_time(func, n):
    start_time = time.time()
    result = func(n)
    end_time = time.time()
    return end_time - start_time, result


# Діапазони для перевірки
ranges = [10, 100, 1000]

# Перевірка продуктивності
for r in ranges:
    simple_time, simple_primes = measure_time(lambda x: [i for i in range(x + 1) if is_prime_simple(i)], r)
    sieve_time, sieve_primes = measure_time(sieve_of_eratosthenes, r)

    print(f"Діапазон: {r}")
    print(f"Проста реалізація: {simple_time:.6f} секунд, Прості числа: {simple_primes}")
    print(f"Решето Ератосфена: {sieve_time:.6f} секунд, Прості числа: {sieve_primes}")
    print()