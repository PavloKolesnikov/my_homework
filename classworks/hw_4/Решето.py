# def sieve_of_eratosthenes(n):
#     # Создаем список чисел от 2 до n
#     numbers = list(range(2, n + 1))
#
#     # Помечаем составные числа
#     for i in range(2, int(n ** 0.5) + 1):
#         if numbers[i - 2] != 0:
#             for j in range(i ** 2, n + 1, i):
#                 numbers[j - 2] = 0
#
#     # Возвращаем только простые числа
#     return [num for num in numbers if num != 0]
#
#
# # Пример использования
# lower_bound = 100
# upper_bound = 1000
# prime_numbers = sieve_of_eratosthenes(upper_bound)
#
# print(f"Простые числа от {lower_bound} до {upper_bound}: {prime_numbers}")


#----

def sieve_of_eratosthenes(n):
    # Створюємо список чисел від 2 до n
    numbers = list(range(2, n + 1))

    # Відфільтровуємо прості числа
    for i in range(2, int(n ** 0.5) + 1):
        if numbers[i - 2] != 0:
            for j in range(i ** 2, n + 1, i):
                numbers[j - 2] = 0

    # Повертаємо тільки прості числа
    return [num for num in numbers if num != 0]


# Приклад використання
ranges = [(1, 100)]  # Діапазони пошуку
for lower_bound, upper_bound in ranges:
    prime_numbers = sieve_of_eratosthenes(upper_bound)
    print(f"Прості числа від {lower_bound} до {upper_bound}: {prime_numbers}")   #Вытягивание по индексу через fstring
print()

ranges = [(100, 1000)]
for lower_bound, upper_bound in ranges:
    prime_numbers = sieve_of_eratosthenes(upper_bound)
    print(f"Прості числа : {prime_numbers}")

#Программа для сравнения
import time

def measure_time(func, *args):
    start_time = time.time()
    result = func(*args)
    end_time = time.time()
    return result, end_time - start_time

lower_bound = 100
upper_bound = 1000

# Виміряємо час для простої реалізації
simple_result, simple_time = measure_time(is_prime_simple, upper_bound)

# Виміряємо час для Решета Ератосфена
sieve_result, sieve_time = measure_time(sieve_of_eratosthenes, upper_bound)

print(f"Час простої реалізації: {simple_time:.6f} секунд")
print(f"Час Решета Ератосфена: {sieve_time:.6f} секунд")


#----Воторой вариант
#программа выполнения простых чисел:
import time

def is_prime_simple(n):
    if n < 2:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True

# Виміряємо час для простої реалізації
start_time = time.time()
is_prime_simple_result = is_prime_simple(1000)
end_time = time.time()

print(f"Час простої реалізації: {end_time - start_time:.6f} секунд")

#Через эрастофена
def sieve_of_eratosthenes(n):
    numbers = list(range(2, n + 1))
    for i in range(2, int(n**0.5) + 1):
        if numbers[i - 2] != 0:
            for j in range(i**2, n + 1, i):
                numbers[j - 2] = 0
    return [num for num in numbers if num != 0]

# Виміряємо час для Решета Ератосфена
start_time = time.time()
prime_numbers = sieve_of_eratosthenes(1000)
end_time = time.time()

print(f"Час Решета Ератосфена: {end_time - start_time:.6f} секунд")

