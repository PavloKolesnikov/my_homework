import time

# Создаем список чисел от 0 до 10
numbers = list(range(11))

# Начинаем измерение времени
start_time = time.time()

# Вычеркиваем все числа, которые делятся на 2
numbers = [num for num in numbers if num % 2 != 0]

# Создаем список для чисел, которые делятся на 3
divisible_by_3 = [num for num in numbers if num % 3 == 0]
# Удаляем числа, которые делятся на 3, из основного списка
numbers = [num for num in numbers if num % 3 != 0]

# Создаем список для чисел, которые делятся на 5
divisible_by_5 = [num for num in numbers if num % 5 == 0]
# Удаляем числа, которые делятся на 5, из основного списка
numbers = [num for num in numbers if num % 5 != 0]

# Завершаем измерение времени
end_time = time.time()

# Выводим результаты
print("Оставшиеся числа:", numbers)
print("Числа, которые делятся на 3:", divisible_by_3)
print("Числа, которые делятся на 5:", divisible_by_5)
print("Время выполнения операции:", end_time - start_time, "секунд")