import time

def sieve_of_eratosthenes(n):
    primes = [True] * (n + 1)
    p = 2
    while (p * p <= n):
        if primes[p]:
            for i in range(p * p, n + 1, p):
                primes[i] = False
        p += 1
    prime_numbers = [p for p in range(2, n + 1) if primes[p]]
    return prime_numbers

# Начинаем измерение времени
start_time = time.time()

# Применяем метод Решето Эратосфена для чисел до 10
result = sieve_of_eratosthenes(10)

# Завершаем измерение времени
end_time = time.time()

# Выводим результаты
print("Простые числа до 10:", result)
print("Время выполнения операции:", end_time - start_time, "секунд")