class Car:
    def __init__(self, color, distance=0):
        self.color = color
        self.distance = distance
        self.base_number = 3.14

    def move(self, moved_distance = 100):
        self.distance = self.distance + moved_distance


car_object_1 = Car("green")
print(car_object_1.color)
print(car_object_1.distance)

car_object_1.move()
print(car_object_1.distance)

car_object_1.move(500)
car_object_1.move(500)
car_object_1.move(1000)
print(car_object_1.distance)








#
# car_object_1.move()
# print(car_object_1.distance)
#
# car_object_1.move()
# print(car_object_1.distance)
#
#
#
