# def my_func():
#     pass
#
#
# class Car:                #Создал класс,в него могут быть переданы властивості і методи.
#     pass
#
#
# car_object_1 = Car()
# car_object_2 = Car()
#
# print(car_object_1)
# Выведет:
# <__main__.Car object at 0x0000020F98247EC0>

# ----
#
# class Car:  # Создал класс,в него могут быть переданы властивості і методи.
#     def __init__(self, color, size):  # Стандартная конструкция
#         self.color = color
#         self.size = size
#
#
# car_object_1 = Car("green", 40)  # Присвоил переменной цвет
#
# print(car_object_1.color)
# print(car_object_1.size)
#
# car_object_2 = Car(color="black", size=10)
#
# print(car_object_2.color)
# print(car_object_2.size)
# #Выведет:
# # green
# # 40
# # black
# 10
# ----


class Car:  # Создал класс,в него могут быть переданы властивості і методи.
    def __init__(self, color, distance=0):  #
        self.color = color
        self.distance = distance
        self.base_number = 3.14

    def move(self, moved_distance=100):
        self.distance = self.distance + moved_distance


car_object_1 = Car("green")  #

print(car_object_1.color)
print(car_object_1.distance)

car_object_1.move()  # Можно запустить функцию на переменной с помощью методов
print(car_object_1.distance)
#
car_object_1.move()              #Увеличит предыдущее значение функции в два раза
car_object_1.move(300)           #Записал переменную,через метод в функцию записал число и оно мне посчитало,с учетом предыдущих функций
car_object_1.move(6600)
print(car_object_1.distance)
# #Выведет:
# green
# 0
# 100
# 7100
