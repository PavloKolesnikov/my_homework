class Car:
    def __init__(self, color, distance=0):
        self.color = color
        self.distance = distance
        self.base_number = 3.14

    def move(self, moved_distance=100):
        self.distance = self.distance + moved_distance

    def change_weather(self, weather):
        if weather == "bad":
            self.distance = self.distance - 30
            print("can not move")
        else:
            print("Im moving")

    #     >
    def __gt__(self, other):
        return self.distance > other.distance

    #      <
    def __lt__(self, other):
        return self.distance < other.distance

    #     >=
    def __ge__(self, other):
        return self.distance >= other.distance

    #     <=
    def __le__(self, other):
        return self.distance <= other.distance


car_object_1 = Car("green")
print(car_object_1.color)
print(car_object_1.distance)
print()

car_object_2 = Car("yellow", 100)
print(car_object_2.color)
print(car_object_2.distance)
print()

result = car_object_1 > car_object_2
print(result)
print()

car_object_1.move(200)
print(car_object_1.distance)

result = car_object_1 > car_object_2
print(result)

car_object_1.change_weather("bad")
print(car_object_1.distance)
