class Car:  # Создал класс,в него могут быть переданы властивості і методи.
    def __init__(self, color, distance=0):  #
        self.color = color
        self.distance = distance
        self.base_number = 3.14

    def move(self, moved_distance=100):
        self.distance = self.distance + moved_distance

car_object_1 = Car("green")
print(car_object_1.color)
print(car_object_1.distance)

while True:                                       #Здесь бесконечная программа
    user_distance = int(input("add distance: "))
    car_object_1.move(user_distance)
    print(car_object_1.distance)                   #Здесь я вывел дистанцию по методу