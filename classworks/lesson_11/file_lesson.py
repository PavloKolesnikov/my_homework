data = {"name":"John", "age": 30, "city": "New York"}


# file = open("my_file.txt", "r")           #Здесь прописывается путь к файлу и режим "r"(чтение)
# content = file.read()                     #Режим чтения,выводятся данные с блокнота
# print(content)
# # file.close()
#
# file = open("my_file.txt", "a")            #a- append добавление.
# content = file.write("\nsecond line")      #Допишет в блокнот с новой строки second line
# print(content)
# file.close()
# #
# file = open("my_file.txt", "r")
# content = file.read()
# print(content)
# file.close()

# file = open("my_file1.txt", "w")
# content = file.write("\nthird line")
# print(content)
# file.close()

# file = open("my_file1.txt", "r")           #Прочитать строку как список строк
# content = file.readlines()
# file.close()
# print(content)
#
#
# with open("my_file1.txt", "r") as file:
#     content = file.readlines()
#     print(content)
# #
