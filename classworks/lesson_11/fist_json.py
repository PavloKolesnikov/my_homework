import json



# data = {"name":"John", "age": 30, "city": "New York"}
# # data = [1,2,3]
# with open("data.json", "w") as file:              #С помощью этой операции я открываю файл в режиме чтения ("w")
#     json.dump(data, file)                        #С помощью этой команды я сюрреализую файл ,появился новый файл data.txt


# Завантаження даних з файлу JSON
# with open("data.json", "r") as file:
#     result = json.load(file)                        # Этой командой я говорю,чтобы он переделал в язык Python
#     print(result)
#     print(type(result))



# # Десеріалізація рядка JSON у об'єкт Python
# json_str = '{"name": "John", "age": 30, "city": "New York"}'
# print(type(json_str))
# data = json.loads(json_str)
# print(data)
# print(type(data))
# print(data["name"])
# print(data["age"])
# print(data["city"])


# # Серіалізація об'єкта Python у рядок JSON
# data = {"name": "John", "age": 30, "city": "New York"}
# json_str = json.dumps(data)
# print(json_str)
# # Серіалізація з відступами
# json_str = json.dumps(data, indent=4)
# print(json_str)




# data = {"a": 1, "b": [2]}
#
# result = json.dumps(data)
# print(result)
# print(type(result))
#




#
import requests
rr = requests.get("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
print(rr)
# result_2 = rr.content
# print(result_2)
# print(type(result_2))
result_2 = rr.content
result_3 = json.loads(result_2)
print(result_3)
print(type(result_3))





# tt = json.dumps(result_2)
# print(type(tt))
# print(tt)
