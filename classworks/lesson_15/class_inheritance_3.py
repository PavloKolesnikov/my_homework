# Inheritance

class Animal:
    def __init__(self, name):
        self.name = name

    def _put_animal_name(self):
        return f"Hello, {self.name} says: "

class Dog(Animal):
    def sound(self):
        print(f"{self._put_animal_name()} Dog barks.")


class Cat(Animal):
    def sound(self):
        print(f"{self._put_animal_name()} Cat meows..")




dog_1 = Dog("Jack")
dog_2 = Dog("Bob")

dog_1.sound()
dog_2.sound()

cat_1 = Cat("Melow")
cat_2 = Cat("Dan")

cat_1.sound()
cat_2.sound()

