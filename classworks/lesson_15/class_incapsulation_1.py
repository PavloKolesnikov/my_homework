class Car:
    def __init__(self, brand, model):
        self.__brand = brand  # Приватне поле
        self._model = model  # Захищене поле
        self.max_speed = 200  # Публічне поле

    def drive():
        # self.__check_fuel()
        # self.__start_engine()  # Приватний метод
        print("Car is driving.")

    def __start_engine(self):
        print("Engine started.")

    def __check_fuel(self):
        print("20 litres")

    def to_dict(self):
        return {"brand":self.__brand, "model": self._model}




tt = {"brand":"Mercedes", "model": "C class"}

my_car_1 = Car("Toyota", "RAV4")
#
# my_car_2 = Car(**tt)                            #С помощью двух звездочек можно распокавать этот файл и записать в dict
#
# gg = my_car_2.to_dict()
# print(gg)
#
# db_car = DBCar(**gg)                           #Сохранение в базу данних
# db_car.save()
# db_car.to_dict()


print(my_car_1.max_speed)
# print(my_car_1._model)
# print(my_car_1._Car__brand)

print()



# my_car_1.drive()
# my_car_1.drive(my_car_1)
# my_car_1.__start_engine()

