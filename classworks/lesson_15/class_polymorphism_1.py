# Inheritance

class Animal:
    def __init__(self, name):
        self.name = name

    def _put_animal_name(self):
        return f"Hello, {self.name} says: "

    def sound(self):
        print(f"{self._put_animal_name()}.")


class Dog(Animal):
    def sound(self):
        print(f"{self._put_animal_name()} Dog barks.")


class Cat(Animal):
    def sound(self):
        print(f"{self._put_animal_name()} Cat meows..")


class Car:
    def __init__(self, name):
        self.name = name
    def drive(self):
        pass

    def sound(self):
        print(f"bip bip.")


Cat("cat_cat")
Dog("my_dog")

game_objects = [
    Dog("my_dog"),
    Cat("cat_cat"),
    Cat("cat_cat"),
    Car("Toyota"),
    Dog("my_dog"),
    Dog("my_dog"),
    Dog("my_dog"),
    Cat("cat_cat")
]

for animal in game_objects:
    animal.sound()

# def __add__():
#     pass
#
# print(1.add(1))
# print("gg".add("hhh"))
# print([1,2].add([33,5]))

