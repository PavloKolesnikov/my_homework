# Inheritance

class Animal:
    def __init__(self, name):
        self.name = name

    def _specific_sound(self):
        return "Animal sound"

    def sound(self):
        print(f"{self.name} {self._specific_sound()}.")


class Dog(Animal):
    def _specific_sound(self):
        return "Dog barks"

class Cat(Animal):
    def _specific_sound(self):
        return "Cat meows"



dog_1 = Dog("Jack")
dog_2 = Dog("Bob")

dog_1.sound()
dog_2.sound()

cat_1 = Cat("Melow")
cat_2 = Cat("Dan")

cat_1.sound()
cat_2.sound()
# cat_2.

cat_2._specific_sound()









class Animal:
    def __init__(self, name):
        self.name = name

    def sound(self):
        print("Animal makes a sound.")


# Похідний клас, який успадковує Animal
class Dog(Animal):
    def sound(self):
        print("Dog barks.")


# Похідний клас, який успадковує Animal
class Cat(Animal):
    def sound(self):
        print("Cat meows.")


# Створення об'єктів класів
animal = Animal("Animal")
dog = Dog("Dog")
cat = Cat("Cat")

# Виклик методів
animal.sound()  # Виведе: Animal makes a sound.
dog.sound()  # Виведе: Dog barks.
cat.sound()  # Виведе: Cat meows.
