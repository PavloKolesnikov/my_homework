from abc import ABC, abstractmethod

class Printable(ABC):
    @abstractmethod
    def print(self):
        pass

class Shape(ABC):
    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimeter(self):
        print()


class Triangle(Shape, Printable):
    def __init__(self):
        self.a =1
    def print(self):
        print(self.a)
    def area(self):
        # pass
        print("aria")

    def perimeter(self):
        print("perimeter")


class MyTriangle(Triangle):
    def __init__(self):
        # super().__init__()
        self.b =33

    def hello(self):
        print("hello")


rr = Triangle()
rr.print()

gg = MyTriangle()

# rr.area()
# rr.perimeter()
#
# rr = MyTriangle()
# rr.area()
# rr.perimeter()
# rr.hello()
