class MyCar:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.d = 222

    def __str__(self):
        # return f"MyCar({self.a=},{self.b=})"
        return f"<MyCar(a={self.a},b={self.b}) instance>"

    def __repr__(self):
        return f"<MyCar({self.a},{self.b})>"


car_1 = MyCar(1, 4, 66)
car_2 = MyCar(a=33, b=55, c=77)
car_3 = MyCar(a=7777, b="jjjj", c=888)

print(car_1)
print(car_2)

print()
print(car_2.c)
print()

my_list = [car_1, car_2, car_3]
print(my_list)


#
# def staticmethod(fn):
#     def wrapper():
#         result = fn(0,0)
#         print(result)
#         return result
#     return wrapper
#
#
#
# @staticmethod
# def add_numbers(a,b):
#     return a+b
#
#
# # result = add_numbers(1,4)
# # result = add_numbers(6,4)
# # result = add_numbers(199,4)
# result = add_numbers()
