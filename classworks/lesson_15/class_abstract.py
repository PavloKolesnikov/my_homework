from abc import ABC, abstractmethod


class Shape(ABC):
    @abstractmethod
    def area(self):
        pass

    def perimeter(self):
        print()


class Triangle(Shape):
    def __init__(self):
        self.a =1
    def area(self):
        # pass
        print("aria")

    # def perimeter(self):
    #     print("perimeter")


class MyTriangle(Triangle):
    def __init__(self):
        # super().__init__()
        self.b =33

    def hello(self):
        print("hello")


rr = Triangle()
rr.area()
rr.perimeter()

rr = MyTriangle()
rr.area()
rr.perimeter()
rr.hello()
