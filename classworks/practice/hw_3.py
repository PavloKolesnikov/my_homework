# data_1 = (
#     ("student_1", 78, "Failed"),
#     ("student_2", 97, "Passsed"),
#     ("student_3", 86, "Passed"),
#     ("student_4", 67, "Passed"),
#     ("student_5", 78, "Passed")


# data_2 = (
#     ("student_1", 84, "Passed"),
#     ("student_2", 78, "Passsed"),
#     ("student_3", 65, "Failed"),
#     ("student_4", 90, "Passed"),
#     ("student_5", 72, "Failed")
# )

# input_data = data_1
#
# for student_data in input_data:
#     grade = student_data[1]
#     print(grade)




# Создаем кортежи для двух студентов
student1 = (78, "Failed")
student2 = (75, "Passed")


# Список студентов
students = [student1, student2]

# Проверяем последовательность оценок
grades = [student[0] for student in students]
if sorted(grades) == grades:
    print("Профессор был последователен")
else:
    print("Профессор не был последователен")

