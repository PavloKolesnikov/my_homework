# # def my_func():                           #Пишу функцию
# #     print("Hello")
# #
# # my_func()                                #Вызов функции.Можно вызывать сколько хочешь раз.
#
# #----
# #
# # def my_func():                           #Пишу функцию
# #     print("Hello")
# #
# # for i in range(100):                     #Выведет 100 раз Hello
# #     my_func()
# #
# #----
# #Функция упрощает написание кода,чтобы не было написано много строк
#
# user_input = input("your name")
# print("Thank you")
# print("please put your next input")
#
#
# user_input = input("your age")
# print("Thank you")
# print("please put your next input")
#
#
# user_input = input("Your second name")
# print("Thank you")
# print("please put your next input")
#
#
# user_input = input("your id number")
# print("Thank you")
# print("please put your next input")       #Вариант обычного написания

#Вариант написания через функцию:
def thank_you_function():                           #Пишу функцию.Называю функцию по тому,что она выполняет
    print("Hello")

user_input = input("your name")
thank_you_function()

user_input = input("your age")
thank_you_function()

user_input = input("Your second name")
thank_you_function()

user_input = input("your id number")
thank_you_function()

#----ПОКА МЫ ФУНКЦИЮ НЕ ОБЬЯВИЛИ,ОНА НЕ будет работать

#----
# #Импортирую функцию с файла functions_file
# from functions_file import thank_you_function
#
# user_input = input("your name")
# thank_you_function()
#
# user_input = input("your age")
# thank_you_function()
#
# user_input = input("Your second name")
# thank_you_function()
#
# user_input = input("your id number")
# thank_you_function()
