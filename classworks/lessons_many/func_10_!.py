#Первый вариант написания
# def get_sorted(**values):      #Программа принимает только ключ-значение.
#     print(values)
#     return values
#
#
# get_sorted(a=1,b=2,vvv=3)       #Возможно так записать ,чтобы приняло значения
# #!Выведет так:
# #{'a': 1, 'b': 2, 'vvv': 3}

#----

def get_sorted(*, a, b, c, d):     #Передаю в режиме ключ-значение.Наша функция не принимает позиционных аргументов
    result = a + b + c + d
    print(result)
    return result

user_data = {"a":1, "b":2, "c":3, "d":4}

get_sorted(a=user_data["a"], b=user_data["b"], c=user_data["c"], d=user_data["d"]) #Запись говорит о том,что возьми с dict по ключу и выведи.Это заптсь длинная

get_sorted(**user_data)    #ЗАпись говорит о том,что возьми мой dict и распакуй его

#Выведет:
#
