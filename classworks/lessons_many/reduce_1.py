from functools import reduce                    #Программа перемножает между собой предыдущие числа

# my_data = [1, 2, 3, 4, 5]
# result = reduce(lambda x, v: x * v, my_data)
# print(result)
# #Выведет :120

#----
def dd(x,y):
    print(x)
    print(y)
    print()
    return x*y

my_data = [10, 20, 30, 40, 50]
result = reduce(dd, my_data, 100)
print(result)

#Выведет:
# 100
# 10
#
# 1000
# 20
#
# 20000
# 30
#
# 600000
# 40
#
# 24000000
# 50
#
# 1200000000
# -----

