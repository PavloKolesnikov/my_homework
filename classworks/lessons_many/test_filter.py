import timeit

code1 = """
my_data = range(1_000)
dd = lambda element: element % 2 == 0
result = list(filter(dd, my_data))
"""
code2 = """
my_data = range(1_000)
result_1 = []
for element in my_data:
    if element % 2 == 0:
        result_1.append(element)
"""
# Використання timeit для вимірювання часу виконання
time1 = timeit.timeit(stmt=code1, number=10000)
time2 = timeit.timeit(stmt=code2, number=10000)
print(f"Time for algorithm1: {time1} seconds")
print(f"Time for algorithm1: {time2} seconds")