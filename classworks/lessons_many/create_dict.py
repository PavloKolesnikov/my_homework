# a = {"a": 1, "b": 33}     #В основном используется этот метод
# print(a)
#
# b = dict(ff=22, gg="rr")
# print(b)
#
# c = dict.fromkeys([1,2], None)    #Создал два ключа пустых,можно потом добавить информацию
# print(c)

#----

# a = {"a": 1, "b": 33}     #В основном используется этот метод
# print(a)
#
# b = dict(ff=22, gg="rr")
# print(b)
#
# c = dict.fromkeys(range(30), None)    #Создал полследовательно 30 ключей
# c[0] = "new value"                      #Обращаюсь через индекс,но ключю и записываю в него информацию
# print(c)

#----

# a = {"a": 1, "b": 33}
# b = a.copy()                   #Сделал копию dict .У них будут разные ячейки памяти
# print(a)
# print(id(a))
#
# print(b)
# print(id(b))

#----

# a = {"a": 1, "b": 33}
# print(a)
# a.clear()                      #Очистит dict
# print(a)

#----

# a = {"a": 1, "b": 33}
#
# result = a["b"]            #выведет значение ключа "b"
# print(result)

#----

# a = {"a": 1, "b": 33}
#
# result = a.get("a", "not found")           #Достань нам занчения а.Если а НЕТ,то напишется текст not found
# print(result)

#----

# a = {"a": 1, "b": 33}

# print(a)
# result = list(a.keys())                            #Покажи мне все ключи и преврати в список
# result_2 = list(a.values())                        #Покажи мне информацию ключей
# result_3 = list(a.items())                           #Покажи мне значения ключа и ее информацию в скобках
# print(result)

#----

# a = {"a": 1, "b": 33}
# print(a)
# poped = a.pop("b")                                 #Удали ключ b и он его выедет на экран с информацией
# print(poped)
# print(a)

#----
#
# a = {"a": 1, "b": 33}
# b = {3: 3, 4: 4, 5: 5}
#
# print(a)
# a.update(b)                 #Обнови наш dict a данными с b
#
# print(a)

#----

# a= {}
# print(a)
# result = a.setdefault("a", 11)     #Просим найти ключ a,если нет записываются данные в dict новые.Если дальше щаписывать данные в а,то они не изменятся,потому что есть а
# print(result)
# print(a)

#----

# a = {"a": 1, "b": 33}
# result = len(a)                 #Проверяет сколько ключей
# print(result)

#----

# my_dict = {"a": 1, "b": 33}
# result = "b" in my_dict           #Есть ли ключ b
# print(result)

#----

# my_dict = {"a": 1, "b": 33}
#
# if "c" in my_dict:
#     result = my_dict["c"]         #Есть ли ключ "c"
# else:
#     result = "not found"
#
# print(result)

#----

# data = [1, 2, 3]
# for element in data:
#     print(element)

#----

# data = {1:111, "a":3333, 44:55}
# for element in data.values():           #Выведи все значения с ключей
#     print(element)

#----

# data = {1:111, "a":3333, 44:55}
#
# result = list(data.items())            #Достань ключи и значения
# print(result)

#----

# data = {1:111, "a":3333, 44:55}
# for key, value in data,items():
#     print(f"data key is: {key}, and value: {value}")

#----

