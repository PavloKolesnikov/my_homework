# def add_numbers(number_1, number_2):
#     result = number_1 + number_2
#     return                                      #Запись в функции вместо принт.
#
# function_result = add_numbers(2, 3)
#
# print(function_result)

#----

def add_numbers(number_1, number_2):
    result = number_1 + number_2
    return


added_numbers_1 = add_numbers(2, 3)

added_numbers_2 = add_numbers(5, 6)

added_numbers_3 = add_numbers(added_numbers_1, added_numbers_2)

print(added_numbers_3)                                         #пезультат двух функций
