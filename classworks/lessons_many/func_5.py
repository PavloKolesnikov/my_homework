#Параметри по замовчуванню
#
# def pow_numbers(number_1, number_2):
#     result = pow(number, step)                  #Возведение в степень.
#     return result
#
#
# added_numbers = pow(2, 3)
#
# print(added_numbers)

#----

def pow_numbers(number, step=2):                #Здесь в чтеп записываются параметры по умолчанию.Если не вписывать степ,то она везде возведет во вторую степень
    result = pow(number, step)                  #Но если указать степень, то возведет в указанную
    print(result)
    return result

added_numbers = pow_numbers(2)
added_numbers = pow_numbers(5)
added_numbers = pow_numbers(3, 3)
added_numbers = pow_numbers(6, 4)
added_numbers = pow_numbers(5)
added_numbers = pow_numbers(5)
