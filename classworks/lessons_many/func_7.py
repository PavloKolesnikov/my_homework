# def get_sorted(values: list):              #В данном случае в скобках написано,что нужно передать в списке значения.
#     result = sorted(values)
#     print(result)
#     return result
#
#
# get_sorted([1, 3, 2, 10, 5, 6])

#НО можно упростить написание и передать значения(ключи) через *-значить собери и обьедени
def get_sorted(*values):
    result = sorted(values)
    print(result)
    return result

get_sorted(2,5,10,8,9)

