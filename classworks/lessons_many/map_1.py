# my_data = [1, 2, 3, 4, 5, 6]
# result = map(lambda x: x * 10, my_data)
#
# print(list(result))
# #Выведет:
# [10, 20, 30, 40, 50, 60]

#----
#Один вариант написания:
# my_data = {"student_1": ["passed", 80], "student_2": ["passed", 70], "student_3": ["failed", 20]}
# result = map(lambda k: my_data[k][1]+5, my_data)
#
# print(list(result))
# print(my_data)
#
# #Выведет:
# [85, 75, 25]
# {'student_1': ['passed', 80], 'student_2': ['passed', 70], 'student_3': ['failed', 20]}

#----

# def my_func(k):
#     new_value = my_data[k][1] + 5
#     my_data[k][1] = new_value
#     return new_value
#
# my_data = {"student_1": ["passed", 80], "student_2": ["passed", 70], "student_3": ["failed", 20]}
# result = map(my_func, my_data)
# result_2 = filter(lambda x:x != 25, my_data)
#
# print(list(result))
# print(my_data)
# print(list(result_2))
# #Выведет:
#[85, 75, 25]
# {'student_1': ['passed', 85], 'student_2': ['passed', 75], 'student_3': ['failed', 25]}
# ['student_1', 'student_2', 'student_3']

#----
def my_func(k):
    new_value = my_data[k][1] + 5
    my_data[k][1] = new_value
    return new_value

my_data = {"student_1": ["passed", 80], "student_2": ["passed", 70], "student_3": ["failed", 20]}
result = list(map(my_func, my_data))

result_2 = filter(lambda x:my_data[x][0] == "passed", my_data)

print(list(result_2))
print(my_data)
#Выведет:
# ['student_1', 'student_2']
# {'student_1': ['passed', 85], 'student_2': ['passed', 75], 'student_3': ['failed', 25]}

#----

#my_data = [1, 2, 3, 4, 5, 6]
result = map(lambda x: x%2==0, my_data)

print(list(result))
#Выведет:
#[False, True, False, True, False, True]