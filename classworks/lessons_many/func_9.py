def get_sorted(*values):
    result = sorted(values)
    print(result)
    return result


# get_sorted([1, 3, 2, 10, 5, 6])
# get_sorted(2,5,7,1,4,9,10, name = "ihor")
# get_sorted("ihor", 2,5,7,1,4,9,10)


my_data = [1, 2, 0, 10, 3]
#
get_sorted(*my_data)                   #Звездочка значит,что мы будем передавать значения через запятую,без ключей.
# get_sorted(1, 2, 0, 10, 3)
# get_sorted(values=my_data)
