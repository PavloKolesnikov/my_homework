# https://stackoverflow.com/questions/6591931/getting-file-size-in-python


import os
from pprint import pprint

def get_size_in_kb(dir_or_file_path):                 #Функция принимает путь
    size = os.path.getsize(dir_or_file_path)          #Создаем переменную и вытягиваем размер файла с папки ли файла
    size_kb = round(size / 1024, 3)
    return size_kb


list_dir_data = os.listdir('../../')
# print(type(list_dir_data))

files_list = []
directories_list = []
data_dict = {}

for dir_or_file_name in list_dir_data:
    dir_or_file_path = f"../../{dir_or_file_name}"

    if os.path.isfile(dir_or_file_path):
        files_list.append(dir_or_file_name)

        size = os.path.getsize(dir_or_file_path)           #Передаем путь
        size_kb = get_size_in_kb(dir_or_file_path)
        data_dict[dir_or_file_name] = {"path":dir_or_file_path}      #Не понимаю ЭТОГО!!!!!!!!
        print(size_kb)
    else:
        directories_list.append(dir_or_file_name)

pprint(data_dict)