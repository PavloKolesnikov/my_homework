my_data = [1, 2, 3, 4, 5]                         #Отфильтровал значения,чтобы были не парные
result = filter(lambda x: x % 2 != 0, my_data)
print(list(result))                               #Без list будет написано <filter object at 0x000001C2914337C0>

#----
# my_data = [1, 2, 3, 4, 5]
# result = filter(lambda x: x != 4 and x != 1, my_data)
# print(list(result))

# my_data = [1, 2, 3, 4, 5]                                    #Возьми мне первое число с my_data и проверь True или False
# result = filter(lambda x: x % 2 != 0, my_data)
# for i in result:
#     print(i)


# print(list(result))

# my_data = [1, 2, 3, 4, 5]
# divided_by_2 = filter(lambda x: x % 2 != 0, my_data)                #Делятся на 2
# not_3_data = filter(lambda x: x != 3, divided_by_2)                 #Беру предыдущие данные и проверяю
# for i in not_3_data:
#     print(i)


# my_data = [1, 2, 3, 4, 5]
# divided_by_2 = filter(lambda x: my_data[10], my_data)
