# def check_user_is_admin(user_name):                   #Проверка на админа.простая программа.Здесь много return, поэтому лучше использовать следующую программу.
#     if user_name == "admin" :
#         return True
#     else:
#         return False
#
# is_user_admin = check_user_is_admin("ddd")
# print(is_user_admin)

#----

# def check_user_is_admin(user_name):                   #Программа для проверки на админа
#
#     if user_name == 'admin':
#         result = True
#     else:
#         result = False
#
#     return result
#
# user_input = input()
# is_user_admin = check_user_is_admin(user_input)            #Создал переменную и проверил её через функцию.
# print(is_user_admin)


#----
def check_user_is_admin(user_name):                       #Лучший вариант для написания
    result = user_name == "admin"
    return result


user_input = input()
is_user_admin = check_user_is_admin(user_input)
print(is_user_admin)

