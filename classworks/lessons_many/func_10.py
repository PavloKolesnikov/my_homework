# def get_sorted(**dd):            #Это значит,что мы будем принимать данные в dict, то есть ключ-значение
#     print(dd)
#     return dd
#
# get_sorted(a=1,b=2,vvv=3)


def create_user_in_database(*, name, age, email, addres):
    result = name + age + email + addres
    print(result)
    return result


user_data = {"name":'ihor', "age":"70", "email":"sdf@mail.com", "addres":"street"}
user_data2 = {"name":'ihor', "age":"70", "email":"sdf@mail.com", "addres":"street"}
user_data3 = {"name":'ihor', "age":"70", "email":"sdf@mail.com", "addres":"street"}

create_user_in_database(name=user_data["name"], age=user_data["age"], email=user_data["email"], addres=user_data["addres"])
create_user_in_database(**user_data)                  #Распакуй
# create_user_in_database('ihor', "70","sdf@mail.com", "street")


def create_user_in_database(name, age, email, addres):
    result = name + age + email + addres
    print(result)
    return result


user_data = {"name":'ihor', "age":"70", "email":"sdf@mail.com", "addres":"street"}
user_data2 = {"name":'ihor', "age":"70", "email":"sdf@mail.com", "addres":"street"}
user_data3 = {"name":'ihor', "age":"70", "email":"sdf@mail.com", "addres":"street"}

create_user_in_database(name=user_data["name"], age=user_data["age"], email=user_data["email"], addres=user_data["addres"])
create_user_in_database(**user_data)
create_user_in_database(user_data["name"], user_data["age"], user_data["email"], user_data["addres"])


my_new_data = ['ihor',"70", "sdf@mail.com","street"]

create_user_in_database(*my_new_data)
