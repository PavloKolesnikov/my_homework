my_data = [1, 2, 3, 4]

result = list(map(lambda x: x * 10, my_data))
print(result)
print(result)

result_1 = []
for element in my_data:
    temp_result = element * 10
    result_1.append(temp_result)

print(result_1)
#Выведите:
# [10, 20, 30, 40]
# [10, 20, 30, 40]
# [10, 20, 30, 40]

#----
my_data = [1, 2, 3, 4,5,6]

# result = list(map(lambda x: x*10, my_data))
# print(result)
#
#
# result_1 = []
# for element in my_data:
#     result_1.append(element*10)
# print(result_1)





result = list(filter(lambda element: element % 2 == 0, my_data))
print(result)

result_1 = []
for element in my_data:
    if element % 2 == 0:
        result_1.append(element)
print(result_1)



def is_even(element):
    return element % 2 == 0

result = list(filter(is_even, my_data))
print(result)

result_1 = []
for element in my_data:
    if is_even(element):
        result_1.append(element)
print(result_1)


