# import pprint             #Импорт библиотеки pprint

from pprint import pprint   #Второй вариант импорта
a = {1:111, "a":3333, 44:55}
print(a)
pprint(a)

#----

# import pprint as pp             #Первый вариант
#form pprint import pprint as pp  #второй вариант
# a = {1: 1, 2: 22}
# print(a)
# pp(a)

#----
