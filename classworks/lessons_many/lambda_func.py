def my_func(a, b):                            #Простейшая функция.Можно записать через лямбду(следующая запись)
    result = a + b
    return result



my_lambda = lambda a, b: a + b            #Прими какую-то переменную (в нее мы записываем люмбда функцию).Сначала ключевое слово lambda,далее переменную которую он принимает
                                          #далее функцию

result_1 = my_func(1, 2)            #Два варианта записи
result_2 = my_lambda(1,2)

print(result_1)
print(result_2)