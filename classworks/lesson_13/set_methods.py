my_set = {1, 'a', 55, (1, 2), "tt", (1, 2), 'a'}
print(my_set)

print(len(my_set))

# result = 'a' in "nafcd"
# result = 'q' in "nafcd"
# print(result)

# result = 55 in my_set
# print(result)

set_1 = {1, "a", 1, 1, 2 }
set_2 = {1, 2, "a", "a", "a", "a", "a"}
# set_2 = {1, 2, "1", "a", "a", "a", "a", "a"}

result = set_1 is set_2
print(result)

result = set_1 == set_2
print(result)

a = None
print(None)
