# set_1 = {1, "a", 55}
# set_2 = {3, "b", 55}
#
# result = set_1.union(set_2)    #Обьеденить сеты.Дубликаты занчений не создаются!
# print(result)
# #Выведет:
# # {1, 'a', 3, 55, 'b'}
# result= set_1.intersection(set_2)    #Общие находит
# print(result)
# #Выведет:
# # {55}
# result = set_1.difference(set_2)    #Найди все что есть в сете 1 и нет в сете 2
# print(result)
# #Выведет:
# #{1, 'a'}

#----
# set.union
# set.intersection
# set.difference


set_1 = {1,4, "a", 55}
print(set_1)
set_2 = {3, "b", 4, 55}
print(set_2)
print()

result = set_1.union(set_2)
print("result = set_1.union(set_2)")
print(result)
print()

result = set_1.intersection(set_2)
print("result = set_1.intersection(set_2)")
print(result)
print()

result_1 = set_1.difference(set_2)
print("result_1 = set_1.difference(set_2)")
print(result_1)
print()

result_2 = set_2.difference(set_1)
print("result_2 = set_2.difference(set_1)")
print(result_2)
print()

dd = result_1.union(result_2)
print("dd = result_1.union(result_2)")
print(dd)
print()


result = set_1.difference(set_2).union(set_2.difference(set_1))
print("result = set_1.difference(set_2).union(set_2.difference(set_1))")
print(result)

#Выведет:
#{1, 4, 'a', 55}
# {3, 4, 'b', 55}
#
# result = set_1.union(set_2)
# {1, 3, 4, 'b', 'a', 55}
#
# result = set_1.intersection(set_2)
# {4, 55}
#
# result_1 = set_1.difference(set_2)
# {1, 'a'}
#
# result_2 = set_2.difference(set_1)
# {3, 'b'}
#
# dd = result_1.union(result_2)
# {1, 3, 'b', 'a'}
#
# result = set_1.difference(set_2).union(set_2.difference(set_1))      #Можно одной такой переменной записать,чтобы не делать предыдущие записи
# {1, 3, 'b', 'a'}
