# Оптимизированный способ чтобы делать списки и dict

# data = [0, 1, 2, 3, 4, 5]
# for i in data:
#     print(i)


# result = [i*2 for i in data]         #Любой элемент в списке

# print(result)
#Выведет:
# [0, 2, 4, 6, 8, 10]

#----

# data = [0, 1, 2, 3, 4, 5]
#
# result = [f"hello {i}" for i in data]   #Запишет к каждому элементу hello.
#
# print(result)
# #Выведет:
# ['hello 0', 'hello 1', 'hello 2', 'hello 3', 'hello 4', 'hello 5']

#----


# data = [1, 2, 3, 4, 5,]


# result = []
# for i in data:
#     result.append(i*2)
#     print(result)
#
# result = [element for element in data if element % 2 == 0]
# print(result)
#Выведет:
# [2, 4]

# result_1 = [i*2 for i in data]      #Таже запись что и выше,но эта лучше!
# print(result_1)
#Выведет:
# [2]
# [2, 4]
# [2, 4, 6]
# [2, 4, 6, 8]
# [2, 4, 6, 8, 10]
# [2, 4, 6, 8, 10]     #Это result_1




#----



#Это инфа с урока
# data1 = [1, 2, 3, 4, 5, 6]
# data2 = [33,6,999,11,8]
# all_data = [data1, data2]
#
# result = [
#     str(element)
#     for data in all_data
#     for element in data
#     if element % 2 == 0 and element != 4
# ]
# print(result)
#
#
# new_list = []
# for data in all_data:
#     for element in data:
#         if element % 2 == 0 and element != 4:
#             new_list.append(str(element))
#
# print(new_list)
#
#



# result = []
# for i in data:
#     result.append(i*2)
# print(result)
# print(data)
#
#
# result_1 = [i*2 for i in data]
# print(result_1)
# print(data)


#
#
# result = [f"hello {i}" for i in data]
# print(result)


# result = [i*2 for i in data]
# print(result)


