# # result = {i:str(i) for i in range(4)}
# # print(result)
# # (1, 145)
#
# data = {
#     1: 145,
#     2: 446,
#     3: 9,
#     4: 32
# }
#
# even_key_dit = {k: v for k, v in data.items() if v % 2 == 0}
# print(result)
#
from typing import Iterable


def my_func():
    """
    this function prepares data
    my_func(3)   -> 55
    my_func(3)   -> 55

    :return:
    """

def my_func(a: int, b: Iterable) -> None:
    """

    :param a: first value
    :param b:
    :return:
    """



print(my_func.__doc__)
