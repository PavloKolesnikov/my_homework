# my_list = ["d", "h","v"]
# result = my_list[0]
# print(result)


my_list = {"d", "h", "v", 1, 5, 0.5, 1}            #По индексу нельзя обращатся
print(len(my_list))
print(my_list)


my_list = ["d", "h", "v", 1, 5, 0.5, 1]
my_set = set(my_list)                              #Превращаю список в set
print(len(my_set))
print(type(my_set))
print(my_list)


my_list_2 = list(my_set)
print(my_list_2[-1])
print(type(my_list_2))


