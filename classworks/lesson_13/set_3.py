# set_1 = {1,2,3}
# set_2 = {1,2,3,4,5}
#
#
# result = set_1.issubset(set_2)             #проверка на True
# print(result)
#
# result = set_2.pop()                       #Удалить элемент из сета_2
#
# print(set_2)
# #Выведет:
# # True
# # {2, 3, 4, 5}

#----
set_1 = {1,2,10}
set_2 = {1,2,3,4,5}



# result = set_1.issubset(set_2)
# print(result)
#
# result = set_2.pop()
# print(result)
#
# result = set_2.pop()
# print(result)
#
# result = set_2.pop()
# print(result)
#
# print(set_2)


result = set_1.intersection(set_2)             #Тоже самое что и ниже.Лучше такой метод использовать.
print("result = set_1.intersection(set_2)")
print(result)
print()

result_1 = set_1 & set_2                      #Тоде самое что и выше
print("result_1 = set_1 & set_2")
print(result_1)
#Выведет:
# result = set_1.intersection(set_2)
# {1, 2}
#
# result_1 = set_1 & set_2
# {1, 2}