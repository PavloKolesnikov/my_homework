import os

result = os.getcwd()      #Программа покажет где находишся
print(result)
# result_2 = os.listdir("./")     #Покажи содержимое папки до поточної папки
# result_2 = os.listdir("../../")   #Выйдет на одну папку вверх
# print(result_2)
# from pprint import pprint
# result_2 = os.listdir("../../lesson_3_4")
# pprint(result_2)



# with open("../../lesson_3_4/program_with_string_mothod_title.py") as file:  #ОТкрываею другой файл через путь
#     result = file.read()
#     print(result)
#     print(type(result))


# path_1 ="../../lesson_3_4"                                                 #Проверка папка ли это
# path_2 ="../../lesson_3_4/program_with_string_mothod_title.py"
#
# result = os.path.isdir(path_2)
# print(result)

#----

from pprint import pprint

# list_dir_data = os.listdir("../../")              #Метод показывает все содержание папки
# print(type(list_dir_data))
#
# files_list = []
# directories_list = []
#
# for dir_or_file_name in list_dir_data:                    #Пройдись по всем файлам в папке
#     if os.path.isfile(f"../../{dir_or_file_name}"):       #Каждый раз я буду проверять следующее имя, через f-string записывается
#         files_list.append(dir_or_file_name)
#     else:
#         directories_list.append(dir_or_file_name)
#
# print("files:")
# print(len(files_list))                                    #Довжина стринги
# # pprint(files_list)
# print()
#
# print("directories")
# print(len(directories_list))
# # pprint(directories_list)
#
#
#
#
#
#
#
# # pprint(list_dir_data)
#
#
#
#
# # with open("../../ggg", "r") as file:
# #     result = file.read()
# #     print(result)
#

