# def check_sequential_scores(data):
#     # Получаем список оценок в порядке добавления
#     scores = [student["score"] for student in input_data.values()]
#
#     # Проверяем, были ли оценки проставлены последовательно
#     for i in range(1, len(scores)):
#         if scores[i] < scores[i - 1]:
#             return False
#
#     return True
#
#
# # Пример использования
# input_data = {
#     "Student_1": {"score": 78, "status": "Failed"},
#     "Student_2": {"score": 82, "status": "Passed"},
#     "Student_3": {"score": 97, "status": "Passed"},
#     "Student_4": {"score": 86, "status": "Passed"},
#     "Student_6": {"score": 75, "status": "Passed"},
# }
#
# result = check_sequential_scores(input_data)
# if result:
#     print("Оценки были проставлены последовательно.")
# else:
#     print("Оценки не были проставлены последовательно.")

#----


def check_sequential_scores(input_data):
    # Получаем список оценок в порядке добавления
    scores = [student["score"] for student in input_data.values()]

    # Проверяем, были ли оценки проставлены последовательно
    for i in range(1, len(scores)):
        if scores[i] < scores[i - 1]:
            return False

    return True

def passing_threshold(input_data):
    # Получаем список оценок студентов, которые сдали экзамен
    passed_scores = [student["score"] for student in input_data.values() if student["status"] == "Passed"]

    # Вычисляем порог сдачи как среднее арифметическое оценок студентов
    threshold = sum(passed_scores) / len(passed_scores)
    return threshold

# Пример использования
input_data = {
    "Student_1": {"score": 78, "status": "Failed"},
    "Student_2": {"score": 82, "status": "Passed"},
    "Student_3": {"score": 97, "status": "Passed"},
    "Student_4": {"score": 86, "status": "Passed"},
    "Student_6": {"score": 75, "status": "Passed"},
}

result = check_sequential_scores(input_data)
threshold = passing_threshold(input_data)

if result:
    print("Оценки были проставлены последовательно.")
else:
    print("Оценки не были проставлены последовательно.")

print(f"Порог сдачи экзамена: {threshold}")