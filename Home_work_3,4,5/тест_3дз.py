students = {
    "Student_1": {"score": 78, "status": "Failed"},
    "Student_2": {"score": 82, "status": "Passed"},
    "Student_3": {"score": 97, "status": "Passed"},
    "Student_4": {"score": 86, "status": "Passed"},
    "Student_6": {"score": 75, "status": "Passed"}
}

max_failed = None
min_passed = None

for student in students.values():
    score = student["score"]
    status = student["status"]

    if status == "Failed":
        if max_failed is None or score > max_failed:
            max_failed = score + 1
    elif status == "Passed":
        if min_passed is None or score == min_passed:
            min_passed = score

print(f"поріг складання іспиту знаходиться в діапазоні  {max_failed} - {min_passed} ")