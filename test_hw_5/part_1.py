# Открываем файл orders.txt в режиме чтения
with open('../hw_5/orders.txt', 'r') as file:
    # Читаем содержимое файла
    content = file.read().split("@@@")

# Разделяем содержимое файла по символу @@@
# elements = content.split('@@@')

# Создаем словарь для подсчета упоминаний каждого элемента
element_counts = {}

# Подсчитываем количество упоминаний каждого элемента
for element in content:
    element = element.strip()  # Удаляем лишние пробелы
    if element in element_counts:
        element_counts[element] += 1
    else:
        element_counts[element] = 1

# Сортируем элементы по количеству упоминаний в порядке убывания
sorted_elements = sorted(element_counts.items(), key=lambda x: x[1], reverse=True)

# Выводим результаты
for element, count in sorted_elements:
    print(f'{element}: {count}')