from collections import Counter

my_list = ['a', 'b', 'a', 'c', 'd', 'b', 'a', 'c', 'e']
count_dict = dict(Counter(my_list))

for element, count in count_dict.items():
    if count > 1:
        print(f"{element} встречается {count} раз(а) в списке.")


#-------------
my_list = ['a', 'b', 'a', 'c', 'd', 'b', 'a', 'c', 'e']
count_dict = {}

for element in my_list:
    if element in count_dict:
        count_dict[element] += 1
    else:
        count_dict[element] = 1

for element, count in count_dict.items():
    if count > 1:
        print(f"{element} встречается {count} раз(а) в списке.")

#------
Используя метод .count(): Если вы хотите посчитать количество вхождений определенного элемента в строке или списке, 
вы можете использовать встроенный метод .count(). Вот пример:
my_list = [2, 3, 2, 4, 2, 5]
element = 2
count = my_list.count(element)
print(f"Элемент {element} встречается {count} раз(а) в списке.")        
