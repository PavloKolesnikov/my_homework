def count_elements(filename):
    # Открываем файл в режиме чтения
    with open(filename, 'r') as file:
        # Читаем содержимое файла
        content = file.read()

    # Разделяем содержимое файла по символу @@@
    elements = content.split('@@@')

    # Создаем словарь для подсчета упоминаний каждого элемента
    element_counts = {}

    # Подсчитываем количество упоминаний каждого элемента
    for element in elements:
        element = element.strip()  # Удаляем лишние пробелы
        if element in element_counts:
            element_counts[element] += 1
        else:
            element_counts[element] = 1

    # Возвращаем словарь с результатами
    return element_counts

# Пример использования функции
filename = '../hw_5/orders.txt'
result = count_elements(filename)
for element, count in result.items():
    print(f'{element}: {count}')


#-----Количество элементов в стринге
from collections import Counter


def count_repeating_elements(s):
    # Используем Counter для подсчета количества каждого элемента
    element_counts = Counter(s)

    # Возвращаем словарь с результатами
    return element_counts


# Пример использования функции
string = "aabbccddeeff"
result = count_repeating_elements(string)
for element, count in result.items():
    print(f'{element}: {count}')