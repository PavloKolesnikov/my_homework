import json
import os.path

import requests


class CurrencyRate:
    def __init__(self):
        self.rates_data = self._get_rates_data()

    def _get_rates_data(self):
        url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
        response = requests.get(url)
        if response == 200:
            return response.json()
        return []

    def get_rate(self, currency_code):
        for rate in self.rates_data:
            if rate['cc'] == currency_code:
                return rate['rate']
        return None

    def convert_currency(self, amount, from_currency, to_currency):
        if from_currency == 'UAH':
            rate_to = self.get_rate(to_currency)
            if rate_to:
                return amount / rate_to
        elif to_currency == 'UAH':
            rate_from = self.get_rate(from_currency)
            if rate_from:
                return amount * rate_from
        else:
            rate_from = self.get_rate(from_currency)
            rate_to = self.get_rate(to_currency)
            if rate_from and rate_to:
                return amount * (rate_from / rate_to)
            return None


currency_rate = CurrencyRate()


def save_conversions(conversion_data):
    if os.path.exists('conversions.json'):
        with open('conversion.json', 'r') as file:
            data = json.load(file)
    else:
        data = []

    data.append(conversion_data)

    if len(data) > 10:
        data = data[-10:]

    with open('conversion.json', 'w') as file:
        json.dump(data, file, index=4)