import json
import os.path

import requests


class CurrencyRate:
    def __init__(self):
        self.rates_data = self._get_rates_data()

    @staticmethod
    def _get_rates_data():
        print('_get_rates_data')
        url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
        response = requests.get(url)
        with open('response.json', 'w') as response_from_url:
            response_from_url.write(str(response.json()))
        print('response')
        if response.status_code == 200:
            return response.json()
        return []

    def get_rate(self, currency_code):
        for rate in self.rates_data:
            if rate['cc'] == currency_code:
                return rate['rate']
        return None

    def convert_currency(self, amount, from_currency, to_currency):
        print('convert_currency')
        if from_currency == 'UAH':
            rate_to = self.get_rate(to_currency)
            if rate_to:
                return amount / rate_to
        elif to_currency == 'UAH':
            rate_from = self.get_rate(from_currency)
            if rate_from:
                return amount * rate_from
        else:
            rate_from = self.get_rate(from_currency)
            rate_to = self.get_rate(to_currency)
            if rate_from and rate_to:
                return amount * (rate_from / rate_to)
            return amount * rate_to


currency_rate = CurrencyRate()