from pprint import pprint


def process_file(filename):
    with open(filename, 'r') as file:
        content = file.read()
        content_replaced = content.replace("\n\n", "@@@")
        products_list = content_replaced.split("@@@")

    product_count_dict = {}

    for product_name in products_list:
        if product_name in product_count_dict:  # Проверяет,если элемент в dict.Если нет,то записывается .
            product_count_dict[product_name] += 1
            # product_count_dict[product_name] = product_count_dict[product_name] + 1

        else:
            product_count_dict[product_name] = 1

    pprint(product_count_dict)


process_file('orders.txt')
