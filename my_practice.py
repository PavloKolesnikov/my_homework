#ПАйтон всегда читает файлы с верху вниз.Если нужно имнортировать информацию с другого файла,то пишем команду import "название файла"
#в консоле будет чтение сначала с импортированого кода,потом информация с того,в которым работаю.

# massage = "Hello world!"
# print(massage)
#
# name = "Alice"
# greeting = "Hello," + name + "!"
# print(greeting)
#
# sentence = "The quick brown fox jumps over the lazy dog"
# word = sentence.split(' ')                                #Метод разделения слов в квадратных скобках и в кавычках через запятую
# print(word)
#
# uppercase_message = massage.upper()
# print(uppercase_message)
#
# substring = massage[7:12]                                #Выведет только буквы с 7-го индекса до 12-го.
# print(substring)

#----
#Оператор % -printf

# name = "Alice"
# age = 25
# message = "My name is %s and I am %d years old." % (name,age)  #Через оператор % можно подставить имя и возраст.
# print(message)

#----Метод format()
#Цей метод дозволяє вставляти значення змінних у рядок ,вказуючи їх індекси у фігурних дужках
# name = "Alice"
# age = 25
# message = "My name is {} and I am {} years old.".format(name, age)
# print(message)

#----Використання f-рядків (f-strings)
# name = "Alice"
# age = 25
# message = f"My name is {name} and I am {age} years old"
# print(message)

#----
#
# a = 5
# b = 3
# # Додавання
#
# Operators 2
# c = a + b
# print(c) # Виведе: 8
# # Віднімання
# d = a - b
# print(d) # Виведе: 2
# # Множення
# e = a * b
# print(e) # Виведе: 15
# # Ділення
# f = a / b
# print(f) # Виведе: 1.6666666666666667
# # Ділення за остачею
# g = a % b
# print(g) # Виведе: 2
# # Цілочисельне ділення
# h = a // b
# print(h) # Виведе: 1
# # Піднесення до степеня
# i = a ** b
# print(i) # Виведе: 125

#----Порівняння
# a = 5
# b = 3
# # Рівність
# print(a == b) # Виведе: False
# # Нерівність
# print(a != b) # Виведе: True
# # Більше
# print(a > b) # Виведе: True
# # Менше
# print(a < b) # Виведе: False
# # Більше або дорівнює
# print(a >= b) # Виведе: True
# # Менше або дорівнює
# print(a <= b) # Виведе: False

# x = "Hello"      #Порівняння рядків
# y = "World"
# print(x == y) #Виведе False

# Порівняння списків
# list1 = [1, 2, 3]
# list2 = [4, 5, 6]
# print(list1 == list2) # Виведе: False
# # Порівняння дійсних чисел
# float1 = 3.14
# float2 = 2.5
# print(float1 > float2) # Виведе: True

#----Оператори присвоєння
# Просте присвоєння
# x = 5
# print(x) # Виведе: 5
# # Присвоєння зі збільшенням

# Operators 5
# x += 3
# print(x) # Виведе: 8
# # Присвоєння зі зменшенням
# x -= 2
# print(x) # Виведе: 6
# # Присвоєння зі змноженням
# x *= 4
# print(x) # Виведе: 24
# # Присвоєння з діленням
# x /= 2
# print(x) # Виведе: 12.0
# # Присвоєння з взяттям остачі
# x %= 5
# print(x) # Виведе: 2.0
# # Присвоєння з піднесенням до степеня
# x **= 3
# print(x) # Виведе: 8.0
# # Декілька присвоєнь одночасно
# y = z = 10
# print(y, z) # Виведе: 10 10

#----
# Оператор and
# x = 5
# y = 10
# z = 7
# result1 = x < y and y < z
# print(result1) # Виведе: False
# result2 = x < y and y > z
# print(result2) # Виведе: True
# # Оператор or
# a = 3
# b = 6
# c = 2
# result3 = a > b or b > c
# print(result3) # Виведе: True
# result4 = a > b or b < c
# print(result4) # Виведе: False
# # Оператор not
# is_raining = True
# is_sunny = not is_raining
# print(is_sunny) # Виведе: False
# has_apples = False
# has_no_apples = not has_apples
# print(has_no_apples) # Виведе: True

#----
# Перевірка належності до списку
# numbers = [1, 2, 3, 4, 5]
# print(3 in numbers) # Виведе: True
# print(6 in numbers) # Виведе: False
# print(4 not in numbers) # Виведе: False
# print(6 not in numbers) # Виведе: True
# # Перевірка належності до рядка
# text = "Hello, world!"
# print("Hello" in text) # Виведе: True
# print("python" in text) # Виведе: False
# print("world" not in text) # Виведе: False
# print("Python" not in text) # Виведе: True
# # Перевірка належності до словника
# person = {"name": "John", "age": 30, "city": "New York"}
# print("name" in person) # Виведе: True
# print("gender" in person) # Виведе: False
# print("age" not in person) # Виведе: False
# print("country" not in person) # Виведе: True

#----
# x = [1, 2, 3]
# y = [1, 2, 3]
# z = x
# print(x is y) # Виведе: False, оскільки x і y не є тотожними
# print(x is not y) # Виведе: True, оскільки x і y не є тотожними
# print(x is z) # Виведе: True, оскільки x і z є тотожними
# print(x is not z) # Виведе: False, оскільки x і z є тотожними

#----















# first_variable = "0"

# my_name = "Pavel"               #Простейшая программа
# my_name = my_name + "Kolesnikov"

# print(my_name)


#-----------------------------------------------------------------------------------------------------------------------

# DEFAULT_ITEMS_NUMBER = 3 #Данные которые мы не хотим менять constant
# CHAT_BOT_URL = "https..."  Пример

#-----------------------------------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------------------------------

# user_input = input()   #функция для вода в терминале,все что там пишем,отображается
#
# print(user_input)

#-----------------------------------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------------------------------

# number_1 = input()    # В данном случае у нас в терминале придложит ввести два числа.НО 1-й и 2-й input видется как строка и одно число станет за другим:
# number_2 = input()    # Пример 2(первое) + 5(второе) ,ответ 25 -воспринимается как текст.
# #пропускаем строчку
# result = number_1 + number_2
# #пропускаем строчку
# print(result)

#-----------------------------------------------------------------------------------------------------------------------

#фУНКЦИЯ int -дает программе возможность понять,что введенные значения являются числовыми!Пример:
# number_1 = int(input()) #Эта функция говорит о том,что мы должны взять данные отпользователя и преобразовать в числовые значения
# number_2 = int(input())
#
# result = number_1 + number_2
#
# print(result)
# #Это простейший калькулятор

#-----------------------------------------------------------------------------------------------------------------------

#Если мы хотим разыне математические исчисления,то нам нужно :

# user_input_first_number_1 = int(input())
# user_input_first_number_2 = int(input())
#
# sum_result = user_input_first_number_1 + user_input_first_number_2
# multiply_result = user_input_first_number_1 * user_input_first_number_2
#
# print(sum_result)
# print(multiply_result)
# #В данном случае мы сделали умножение и деление и в RUN просто вводим данные и получаем два ответа

#-----------------------------------------------------------------------------------------------------------------------

#Пример того,как в консоли можно узнать тип данных через функцию type(). 'str'- значит что это строка с текстмом;
#int - что это числовое значение. Так мы узнаем какой класс у элемента

#-----------------------------------------------------------------------------------------------------------------------

#Вернемся к функции input,запись текста,чтобы пользователь понимал что ему нужно ввести:

# user_input_first_number_1 = int(input("input first number and press enter: "))    #Ввели первое число
# user_input_first_number_2 = int(input("input second numberand press enter: "))    #Ввели второе число
#
# sum_result = user_input_first_number_1 + user_input_first_number_2            #написали функцию сложения
# multiply_result = user_input_first_number_1 * user_input_first_number_2       #Написали функцию умножения
# print()                                                                       #Это пустой print() ,он нужен для того.чтобы я мог опустит строку.Для наглядности
#
# sum_result_text = "result of sum operation"                 #Написал переменную текста для отображения функции
# print(sum_result_text)                                      #Написал функцию для вывода текста перед функцией сложения
# print(sum_result)                                           #Вывел результат сложения
#
# print()                                                     #Это пустой print() ,он нужен для того.чтобы я мог опустит строку.Для наглядности
# multiplay_result_text = "result of multiplay operation"     #Написал текст для вывода умножения перед ответом
# print(multiplay_result_text)                                #Вывел текст
# print(multiply_result)                                      #Вывел результат умножения

#-----------------------------------------------------------------------------------------------------------------------

#     string_type- Краткая информация в блокноте о работе в терминале

# my_text = "my text"
#
# result = my_text[-1]                                           #Индекс 0 соответсвует первой букве в переменной my_text
#
# print(result)



# my_text = "my_file.py"                                           #В данном случае в квадратных скобках мы по индексу прописываем диапазон
#                                                                  #информации, которую хотим отобразить.Со 1-го символа по 4-й .
# result = my_text[:7]                                             #Тут сущенствует много разных вариантов, если мы поставим [:]-то оно выведет до конца всю информацию.
# result2 = my_text[7:]                                            #Если [:6] - то все до шестого символа по индексу
# result3 = my_text[:-3]
#
# print(result)                                                    #Выведется два результата: Один до точки,второй после точки согласно индексам.
# print(result2)
# print(result3)                                                   #В данном случае мы увидим файл минус три значения с конца


#-----------------------------------------------------------------------------------------------------------------------

#Використання f-рядків (f-strings)

# hello_text = "hello"
# name = "Pavel"
#
# result_massage = hello_text + " " + name
#
# result_massage_fstring = f"this is {hello_text} {name}"            #В данном случае мы через фигурные скобки обращаемся к переменной, НО обязательно нужно перед сообщением "f" и без плюсов.
#
# print(result_massage)
# print(result_massage_fstring)

#-----------------------------------------------------------------------------------------------------------------------

# a = 1
# b = 3
# result = a + b
# result_text = f"result of {a} + {b} = {result}"      #В данном случае я через f(string) написал текст и прибавил к нему переменную.
#
# print(result_text)

#-----------------------------------------------------------------------------------------------------------------------
                       #МЕТОДЫ

# my_text = "Pavlo Kolesnikov"
# result = my_text.upper()                               #В данном случае после того как я записал переменную my_text в result
# print(result)                                          #и поставил точку,то появился перечень методов.которые можно провести над переменной.
#                                                        #все буквы будут с большой буквы в имени и фамилии.

#----
# user_name = input("your name")                          #!!!Так я попросил ввести имя
# print(user_name.title())                                #Красиво вывело то,что я написал(продублировало)

#----
# my_text = "pavlo kolesnikov"                              #Метод split (разделение)
# print(my_text)
# result = my_text.title()                                  #.title означает,что мы хотим заглавные буквы  в имени и фамилии написать с большой буквы
# print(result)
# result = result.split(" ")                                #метод разделить имя и фамилию через запятую и в квадратных скобках
# print(result)                                             #Вывел результат двух метод result
# name = result[0]                                          #Добавил переменную "name",чтобы через индекс "0" -который соответствует имени в целом,мог вывести на экран только имя
# print(name)                                               #Имею выполнение трех метод .title,.split, result[0]

#----
      #Метод count -посчитать сколько букв тексте переменной (я так понимаю,что можно посчитать символы)
# my_text = "pavlo kolesnikov"
# result = my_text.count("o")
# print(result)
# result = my_text.count("pavlo")                             #Посчитает сколько имен pavlo
# print(result)

# my_text = "pavlo kolesnikov"                                #В данном случае метод найдет с какого индекса начнется фамили (6 индекс)
# result = my_text.find("kolesnikov")
# print(result)

#----
# my_text = "pavlo kolesnikov"                                 #Данный метод позволяем изменить старую фамилию на новую с помощью команды .replace
# result = my_text.replace("kolesnikov", "kuzovkov")
# print(result)

#----
# my_text = "Pavlo Kolesnikov"                                     #функция посчитает сколько символов используется
# result = len(my_text)
# print(result)

#----
# path_to_file = "/my_practice.py"                                 #В данном случае прописан путь к файлу и выведен на экран
# print(path_to_file)
# result = path_to_file.endswith(".py")                            #Здесь программа проверяет через .endswitch заканчивается файл на .py
# print(result)

#----
# user_input = input("input file name: ")                          #Программа для проверки через консоль (вводим данные).является ли файл python
# result = user_input.endswith(".py")
# print(result)

#-----------------------------------------------------------------------------------------------------------------------

#УРОК № 4 : Булевые значени True False

# x = 5                                                             #Простейшая программа на проверку True False. также могу это проделать в терминале
# y = 10
# print(x == y)
# print(x < y)

#----

# user_input = input()                                              #Простейшая программа для проверки ввел ли что-то пользователь.Если ввел,но напишеь введеные данные
# if bool(user_input):                                              #Если не ввел,то напишет empty input
#     print(user_input)
# else:
#     print("empty input")

#----

# input_1 = int(input())                              #При вводе двух чисел я делаю :
# input_2 = int(input())                              #1.Функция деления. result покажет как поделились два числа
#
# result = input_1 / input_2
# result_2 = result - (input_1 // input_2)            #Тут я отнял ое первого результата сколько раз поделилось число и вывел информацию после зяпятой
# print(type(result_2))                               #В данном случае команда значит :Выведи мне какого вида результат через функцию type
# result_3 = str(result_2)[2:]                        #Здесь я преобразовал result_2 в стрингу и через [2:] вывел ТОЛЬКО значения после запятой
#
# print(result)
# print(result_2)
# print(result_3)

#----

# import math                                 #ТАК ИМПОРТИРУЕТСЯ МАТЕМАТИЧЕСКАЯ БИБЛИОТЕКА
# result = math.pow(3,2)                      #Возвести в степень
#
# print(result)

#----

# user_input = input()                          #В данном случае пользователь если введет имя или т.д то напишется то что ввел.
# result = user_input or "user"                 #Если ничего не ввел,то напишется user.Первое True
# print(result)

#----

# user_name = input()                             #Программа,которая просит ввести имя пользователя
#
# is_titled = user_name.istitle()                 #Программа проверяет написано ли имя с большой буквы
# is_name_admin = user_name == "admin"            #Имя пользователя не должно быть admin (== не ровняется)
#
#
# is_valid_name = is_titled and not is_name_admin  #Если имя написано с большой буквы и НЕ имя admin,то сработает True True.Так можно дельть сколько хочешь проверок.
#
# if is_valid_name:                               #Через f-стрингу записываю имя пользователя
#     print(f"Hello {user_name}")
# else:
#     print(f"{user_name} is invalid name")        #Если что-то не совпало (где-то было False),то выведет,что имя не правильное

#----

# user_input_number = int(input())
# is_valid_input = user_input_number > 0 and user_input_number !=256
# is_valid_input = user_input_number > 0 or user_input_number == -300

#-----------------------------------------------------------------------------------------------------------------------

#if-else

 #Простейшая программа:

# user_inpit_1 = int(input())          #Данная программа попросит ввести два числа.Действия будут выполняться согласно условию.
# user_inpit_2 = int(input())
#
# if user_inpit_1 > user_inpit_2:
#     pass                             #pass -место для кода,может быть prent
# elif user_inpit_1 == user_inpit_2:
#     print("a is greater than b ")
# elif user_inpit_1 < user_inpit_2:
#     print("a is less than b")
# else:                                #else-условия ,которое выполнится,если другие не прошли.

#----

# user_name = input()
#
# if user_name == "admin":
#     print("you are admin")
# else:
#     print("you are not admin")

#----

# a = int(input())
# b = int(input())
#
# is_a_greater = a <= b
# a_is_not_23 = a != 23
# b_is_equal_400 = b == 400
# b_is_not_bad_number = is_a_greater and a_is_not_23
#
# if b_is_not_bad_number or b_is_equal_400:
#     print("Ты молодец!")

#----

#match_case

# user_input = int(input())
#
#
# match user_input:
#     case 0:
#         print("Ноль")
#     case 1:
#         print("One")
#     #case _:                  # Проверка до того момента,пока условие не выполнится
#     #   print("Another number")

#----

# user_input_password = int(input("Input your password"))
# database_password = 123
#
# if user_input_password == database_password:
#     user_has_access = True
# else:
#     user_has_access = False

#----

# user_input_password = input("input your password")   #Простейшая программа на проверку пароля.
#
# chek_len = len(user_input_password) > 5
# chek_not_111 = user_input_password != '1111'
#
# password_is_valid = chek_len and chek_not_111
#
# if password_is_valid:
#     print("passwor is valid")
# elif not chek_len:
#     print("len is less than 5, should be more")
# else:
#     print("password is not valid")
#

#----

# try-except                        #Для программиста,выполняется с try,если появится ошибка выведется текст с except.И fanally выполнится.

# try:
#     pass
# except:
#     print("ssasdas")
# finally:
#     print("thank you")

#----

# user_string_input = input()
# element_index = int(input())
#
# try:
#     result = user_string_input[element_index]     #ОБРАЩЕНИЕ по индексу.Запросить вывод индекса
#     print(result)
# except:
#     print("your index is greater than len of your text")
# finally:
#     print("thank you")
#

#----

#datetime

# import datetime #Импортируется библиотека datetime
#
# concurrent_time = datetime.datetime.now()  #Вот так она  прописывается.
# print(concurrent_time)
# print(type(concurrent_time))
#
# print(concurrent_time.year)
# print(concurrent_time.hour)
# print(concurrent_time.day)
#
# another_time = datetime.date(2024, 1, 12)
# print(another_time)
# print(another_time.day)
# time_2 = datetime.datetime(1999, 1, 1, 12, 56, 1)
#

#----
                               #Программа для вывода даты
#
#
# import datetime
#
#
# user_year = int(input("year"))
# user_month = int(input("month"))
# user_day = int(input("day"))
#
# user_time = datetime.datetime(user_year, user_month, user_day)
# print(user_time)
#
# concurrent_datetime = datetime.datetime.now().date()
# print(concurrent_datetime)
#
# result = concurrent_datetime - user_time
# print(result)

#----               #Программа вывода времени

# import datetime
#
# result =datetime.datetime(2000, 2, 2, 0, 10, 13)
# print(result)
# print()
#
# result = datetime.datetime.now()
# print(result)
# print(result.date())
# print(result.time())
# print()

#-----------------------------------------------------------------------------------------------------------------------
#Робота со списками ,урок №6, слайсы
# my_list = [1, 2, 3, 3, 5, "six", "seven"]
#
# my_second_list = []                         #Вывести пустой список
# print(my_second_list)
#
# print(type(my_second_list))

#----

# my_list = [1,2,3,"assdasdafs", 0.222, True]  #Вывести элемент через индекс
#
# result= my_list[4]
# print(result)

#----

# #Вывести инфу со списка через input, выведится элемент из списка
# my_list = [1, 2, 3, "adsasdad", 0.222, True]
#
# user_index = int(input("index: "))

# result = my_list[user_index]
# print(result)

#----
# #Создать список с помощью range
# my_list = list(range(1,10))      #Создаст список с последовательным выводом чисел от 1 до 10(10 в списке не будет)
# print(my_list)
# print(my_list[3])                #Вывод со списка 3-го элемента

#----

# my_list = [1, 2, 3, "adsasdad", 0.222, True]
#
# result = my_list[2]
# print(result)
#
# result = my_list[-1]

#----
#Через len можно определить длинну строки
# my_list = [1, 2, 3, "adsasdad", 0.222, True]
#
# last_index = len(my_list) - 1      #Вывод последнего элемента
# result = my_list[last_index -3]    #Вывод элемента со счета -1,следующий 3 элемент
# print(result)
#
# result = my_list[-1]

#----

# my_list = [1, 2, 3, "adsasdad", 0.222, True]
#
# result = my_list[::-1]               #Переворачивает список и выводит его с шагом -1
# print(result)

#----

# my_list = [1, 2, 3, "adsasdad", 0.222, True, "six", "seven", "abcd", 1, 4,5]
#
# result = my_list[5:1:-1]                #Выведет список с конца с 5-го элемента и до 1 в обратном порядке
# print(result)

#----

# my_list = []
# my_list = [1, 2, 3, "adsasdad", 0.222, True, [1, 2, 3]]
# result = my_list[-1][-1][0]                #В данном случае выводится будут символы от каждого последующиго индекса(сначала в список будет в обратном порядке,потом по индексам )
# print(result)

#----

#Методы списков
# #append- добавляет ф список информация
# my_list = [33,2,"a"]
# print(my_list)
#
# my_list.append(77)
# print(my_list)
#
# my_list.append("sadg")
# print(my_list)
#
# my_list.append([5,6])
# print(my_list)
# #и т.д .Иформация будет записываться последовательно
# #[33, 2, 'a']
# # [33, 2, 'a', 77]
# # [33, 2, 'a', 77, 'sadg']
# # [33, 2, 'a', 77, 'sadg', [5, 6]]   -вот такой список выведится после команд
#----
# #Метод extend
# my_list.extend([10,11,12])
# print(my_list)

# #[33, 2, 'a']
# # [33, 2, 'a', 77]
# # [33, 2, 'a', 77, 'sadg']
# # [33, 2, 'a', 77, 'sadg', [5, 6]]
# # [33, 2, 'a', 77, 'sadg', [5, 6], 10, 11, 12]    -extend -в квадратных скобках добавляются несколько элементов сразу

# my_list_1 = [1,2,3]
# my_list_2 = [4,5,6]
#
# my_list_1.extend(my_list_2)                       #Так можно добавить один список в другой
# print(my_list_1)

#---Метод .join
# result = "s" + "rr" + "rr" + "rr" + "rr"
# print(result)
# result = "".join(["s", "rr", "rr", "rr", "rr"])
# print(result)
#Вывело:
#srrrrrrrr
#srrrrrrrr

#----
# my_list = []          #В данном случае создался пустой список ,потом последовательно переменными заполняем список
# print(my_list)
#
# user_input = input("input list item")
#
# my_list.append(user_input)
# print(my_list)
#
# user_input = input("input your second item")
#
# my_list.append(user_input)
# print(my_list)

#----
# #insert-Выбираем куда вставлять элемент в список
# my_list = [1,2,3]
# # print(my_list)
# #
# # my_list.insert(2,"asd")
# my_list.insert(2, [5,6,7,8])
# print(my_list)
# #[1, 2, [5, 6, 7, 8], 3] -результат второго insert

# #----
# #revers-список наоборот
# my_list = [1, 2, 3]
# my_list.reverse()
# print(my_list)
# #[3, 2, 1]
#
# my_list = [1, 2, 3]
# result = my_list[::-1]
# print(result)
# #То же самое,что и revers

#----
# #sort- сортируют список по очереди
# my_list = [1, 2, 6,10, 3, 5]
# my_list.sort()
# print(my_list)
# #[1, 2, 3, 5, 6, 10]
# #Можно проделывать тоже с буквами
#----
# user_input_1 = int(input())                                #Ввести три числа и отсортировать их по увеличению.Так же можно работать со стрингой.
# user_input_2 = int(input())
# user_input_3 = int(input())
#
# my_list = [user_input_1, user_input_2, user_input_3]
# print(my_list)
#
# my_list.sort()
# print(my_list)

#----
# my_list = [print,11,33,print]
# print(my_list)
#
# my_list[3] ("dddd")
# print("dddd")

#----
#remove-Удаляет со списка элемент
# my_list = [1, 2, "a", 10]
# print(my_list)
#
# my_list.remove("a")
# print(my_list)
#
# my_list.remove(10)
# print(my_list)

#----
#Метод pop-удаляет элемен по индексу и может возвратить его
# my_list = [1, 2, "a", 10]
# print(my_list)
#
# result = my_list.pop(-2)
# print(my_list)                #Выведет список без буквы "a"
# print(result)                 #Выведет "a" отдельно
#Если использовать метод pop() и ничего не прописывать в скобках,удалится последний элемент в списке

#----
#Метод index
# my_list = [1, 2, "a", 10]
# result = my_list.index(10)
# print(result)
#Показывает только индекс элемента первого

#----
# #Метод count
# my_list = [1, 2, "a", 10]
# result = my_list.count(2)
# print(result)
# #Считает сколько двоек в списке

#----
# my_list = [1, 2, "a", 10]
# result = my_list.count(2)
# index_1 = my_list.index(2)
# index_2 = my_list.index(2,index_1+1)    #Поиск со следующего индекса
#
# print(index_1)
# print(index_2)

#----

 #Циклы-----------------------------------------------------------------------------------------------------------------

 #Цыкл for:
 #Вывести все числа на эркан
# my_list = [1,2,3]                #В данном случае создается лист,с числами. for говорит о том,что возьми все переменные и выведи их все в строку
# for my_var in my_list:           #каждую по очереди.Когда цикл закончится,выведется последний print
#      print(my_var)
#
# print("finished")

#----
# user_input = input("your list: ")   #Программа попросит ввести числа
# my_list = user_input.split(",")     #Числа записываются через запятую.Метод .split разделит каждый элемент через запятую и выведет на новой строке.
# for element in my_list:             #elemnt-элементы из списка.Если есть элемент,то программа его умножит на 10.
#     temp_result = int(element)*10
#     print(temp_result)
#
# print("finished")                   #Когда цикл закончится,выведится последний принт.

#----
# your_numbers = [1, 2, 3, 4]
#
# for your_number in your_numbers:
#     #temp_result = int(car)*10
#     print(f"your number is: {your_number}")
#Выведет:
            #your number is: 1
            # your number is: 2
            # your number is: 3
            # your number is: 4

# #----
#Зарплата за 4 месяца
# your_salary_in_4_month = [1000, 2000, 3000, 4000]
#
# for your_month_salary in your_salary_in_4_month:
#     temp_result = int(your_month_salary/1000)   #temp-ТАК НЕ НАЗЫВАЮТ ПЕРЕМЕННУЮ,пример.
#     print(f"{temp_result} тис. долларів")
#
# Виведе:
# 1 тис. долларів
# 2 тис. долларів
# 3 тис. долларів
# 4 тис. долларів

#----
# Так виглядит цикл for:
# for element in [1,2,3]
#     pass
#     print(element)

#----
#Вывести список элементов от и до с помощью range
# for element in range(1,10):
#     print(element)
#выведет:
# 1
# 2
# 3
# 4
# 5
# 6
# 7
# 8
# 9

#----
# for element in range(1,10):
#     print("hello")
# #Выведет:
# hello
# hello
# hello
# hello
# hello
# hello
# hello
# hello
# hello

#----
# for element in "abcd":           #Каждый элемент вытягивается по индексу
#     print(element)

#Выведет:
# a
# b
# c
# d

#----
# for element in [1, 2, 3]:
#     print(element)
# else:
#     print("finished")
#
# #Выведет :      #В основном нам это не нужно
# # 1
# # 2
# # 3
# # finished

#----
#ЦИКЛ В ЦИКЛЕ
# for list_element in ['asd', 'sdf', 'fff']:     #Создали список элементов
#     for str_element in list_element:           #Каждый элемент вытягивается отдельно по индексу
#         print(str_element)                     #Вывожу элементы
#     print()                                    #Разделяю их пустым принтом

#Выведет:
# a
# s
# d
#
# s
# d
# f
#
# f
# f
# f

#----
#Чтобы написать элементы в строку
# result_string = ""
# for list_element in ['asd', 'sdf', 'fff']:
#     for str_element in list_element:
#         result_string = result_string + str_element
#     result_string = result_string + " "
#
# print(result_string)
#
# #Выведется:
# #asd sdf fff

#----
# #Вывести все числа из списка кроме нуля
# for element in [1, 0, 2, 5, 6, 10]:
#     if element !=0:
#         print(element)
#
# # Выведет:
# # 1
# # 2
# # 5
# # 6
# # 10

#----
# # continue-функция,после которой обратно возвращаются к списку и вытягивает следующий элемент
# for element in [1, 0, 2, 5, 6, 10]:
#     if element == 0:
#         continue        #Эта функция в цикле говорит о том , пропусти все что дальше и вернись  продолжи цикл
#     print(element)
#
# #Выведет:
# # 1
# # 2
# # 5
# # 6
# # 10

#----
# for element in ["ihor", "jack", None, "ira"]:
#     if element is None:
#         continue
#     print(element.title())
# #Выведет:
# Ihor
# Jack
# Ira

#----
#Остановить цикл через break ,когда увил в списке слово или цифру,после которой нужно остановится.
# for element in ["ihor", "jack", "exit", None, "ira"]:
#     if element == "exit":
#         break
#     print(element.title())
#
# print("I am not in cycle")
#
# #Выведет:
# Ihor
# Jack
# I am not in cycle

#----
# #Цикл while
# a = 0                #Начальная переменная
# while a < 3:
#     print(a)
#      a += 1          #Добавляет к переменной а единицу и переходит в начало.
#
# print("finished")
#
# #Выведет:
# 0
# 2
# finished

#----

# a = 0
# while True:                   #Выполнится первое условие,потом еще два раза,когда а=3,то сработает break
#     print("asd")
#     a += 1
#     if a == 3:
#         break
#
# print("finished")
#
# #Выведет:
# asd
# asd
# asd
# finished

#----

# for i in range(5):
#     print("asd")
#
# #Выведется:
# asd
# asd
# asd
# asd
# asd

#----
# a = 0                    #Программа идет по циклу.Сначала выведется первый раз принт,потом прибавит 1 и снова выведется.
# while True:              #Цикл ,когда а=3 просто вернется к началу.а когда будет а=5 ,то сработает break.После break дальше ничего не выведется.
#     print("asd")
#     a += 1
#     if a == 5:
#         break
#     if a == 3:
#         continue
#     print("Im working")
#
# #Выведет:
# # asd
# # Im working
# # asd
# # Im working
# # asd
# # asd
# # Im working
# # asd

#----

#Тернарный оператор
# a = 3
# b = 2
# #
# # if a > b:
# #     print("Hello")
# result = "hello" if a > b else "finish"           #Это и есть итернарный оператор .Через строку сделать действие.
# print(result)

#-----------------------------------------------------------------------------------------------------------------------
#Работа с листами
# my_list = [1, 2, 3]
# result = sum(my_list)
# print(result)
#
# my_list = [1, 2, 3]
# result = max(my_list)
# print(result)
#
# my_list = [1, 2, 3]
# result = min(my_list)
# print(result)
#
# #Выведет:
# 6
# 3
# 1

#----
# a = "a,b,c"
# result = a.split(',')
# print(result)
#
# result_2 = "-".join(result)
# print(result_2)
#
# #Выведет:
# ['a', 'b', 'c']
# a-b-c

#----
#Задублировать буквы
# a_input_str = "a,c,d"
# print(a_input_str)
# a_list = a_input_str.split(',')
#
# doubled_list = []
# for element in a_list:
#     temp_result = element+element
#     doubled_list.append(temp_result)
#
# print(doubled_list)
#
# result_2 = ",".join(doubled_list)
# print(result_2)
#
# #Выведет:
# a,c,d
# ['aa', 'cc', 'dd']
# aa,cc,dd

#----
#Кортежи- тот самый список,но изменять мы его не можем.
# my_tuple = (1, 2, 3)
# print(my_tuple)
# print(type(my_tuple))
#
# #Выведет:
# (1, 2, 3)
# <class 'tuple'>

#----
# for i in range(1,101):
#
#     # Если число делится и на 3, и на 5
#     if i % 3 == 0 and i % 5 == 0:
#         print(i, "fizzbuzz")
#     # Если число делится на 3
#     elif i % 3 == 0:
#         print(i, "Да")
#     # Если число делится на 5
#     elif i % 5 == 0:
#         print(i, "no")
#     # Если число не делится ни на 3, ни на 5
#     else:
#         print(i)

#----
# # Пройти через числа от 1 до 100
# for i in range(1, 101):
#     # Преобразовать число в строку, чтобы проверить наличие цифры 7
#     if '7' in str(i) or i % 7 == 0:
#         print("boom")
#     else:
#         print(i)
