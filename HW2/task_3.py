#Це завдання можна виконати двома способами:
#1)

# user_input_first_number = int(input("Please input your first number: "))
# user_input_second_number = int(input("Please input your second number: "))
# user_input_third_number = int(input("Please input your third number: "))
#
# numbers = [user_input_first_number, user_input_second_number, user_input_third_number]
# numbers.sort()
#
# print(numbers[0])
# print(numbers[1])
# print(numbers[2])

#----
#2)

first_number = int(input("Введите первое число: "))
second_number = int(input("Введите второе число: "))
third_number = int(input("Введите третье число: "))

if first_number <= second_number <= third_number:
    print(first_number)
    print(second_number)
    print(third_number)
elif first_number <= third_number <= second_number:
    print(first_number)
    print(third_number)
    print(second_number)
elif second_number <= first_number <= third_number:
    print(second_number)
    print(first_number)
    print(third_number)
elif second_number <= third_number <= first_number:
    print(second_number)
    print(third_number)
    print(first_number)
elif third_number <= first_number <= second_number:
    print(third_number)
    print(first_number)
    print(second_number)
else:
    print(third_number)
    print(second_number)
    print(first_number)