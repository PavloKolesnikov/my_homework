import os

import telebot  # назва пакету відмінна від тієї, що при завантаженні
from dotenv import find_dotenv
from dotenv import load_dotenv

env_file = find_dotenv('../new_bot/.env')
load_dotenv(env_file)

TBOT_TOKEN = os.environ.get("TBOT_TOKEN")

bot = telebot.TeleBot(TBOT_TOKEN)


@bot.message_handler(func=lambda message: True)  # у дужках загальна форма
def handle_message(message: telebot.types.Message):
    # Обробка повідомлення
    bot.reply_to(message, 'HELLO user: ' + message.text)


bot.polling()
