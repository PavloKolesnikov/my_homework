import telebot
import os

import telebot  # назва пакету відмінна від тієї, що при завантаженні
from dotenv import find_dotenv
from dotenv import load_dotenv

env_file = find_dotenv("../new_bot/.env")
load_dotenv(env_file)
TBOT_TOKEN = os.environ.get("TBOT_TOKEN")
bot = telebot.TeleBot(TBOT_TOKEN)


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = telebot.types.ReplyKeyboardMarkup()
    keyboard.row('Button 1', 'Button 2')
    keyboard.row('Button 3', 'Button 4')
    bot.send_message(message.chat.id, 'Виберіть кнопку:', reply_markup=keyboard)


@bot.message_handler(func=lambda message: True)
def handle_button_click(message):
    if message.text == 'Button 1':
        bot.send_message(message.chat.id, 'Ви натиснули кнопку 1')
    elif message.text == 'Button 2':
        bot.send_message(message.chat.id, 'Ви натиснули кнопку 2')
    elif message.text == 'Button 3':
        bot.send_message(message.chat.id, 'Ви натиснули кнопку 3')
    elif message.text == 'Button 4':
        bot.send_message(message.chat.id, 'Ви натиснули кнопку 4')


bot.polling()
