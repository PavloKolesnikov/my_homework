import telebot
import os

import telebot  # назва пакету відмінна від тієї, що при завантаженні
from dotenv import find_dotenv
from dotenv import load_dotenv

env_file = find_dotenv("../new_bot/.env")
load_dotenv(env_file)
TBOT_TOKEN = os.environ.get("TBOT_TOKEN")
bot = telebot.TeleBot(TBOT_TOKEN)


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = telebot.types.ReplyKeyboardMarkup()
    keyboard.row('Button 1', 'Button 2')
    keyboard.row('Button 3', 'Button 4')
    bot.send_message(message.chat.id, 'Виберіть кнопку:', reply_markup=keyboard)


bot.polling()
